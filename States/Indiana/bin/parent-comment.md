### Current flag of [Indiana](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Flag_of_Indiana.svg/1024px-Flag_of_Indiana.svg.png)

---

### Design Process

Current flag of Indiana is pretty good, but they had to write their name on the flag.

For my redesign, I kept the torch which symbolizes liberty, dropeed the stars, and changed the color of torch to a soft, warm red.

And this is the result. Up next is Iowa.

---

### Additional Information

I'm redesigning the state flags of USA because why not. Share your opinion and comments, it'll help me greatly. 

[Catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.

!wave