### Current flag of [Maine](https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Flag_of_Maine.svg/1024px-Flag_of_Maine.svg.png)

---

### Design Process

Current flag of Maine yet another case of seal on a deep blue bedsheet. Complex imagery, text, motto, and 2 dudes, it has got it all.

For my redesign, I created 3 pine trees using 3 triangels; each triangle follows golden ratio. Background is a diagonal split with black and white symbolizing night and day. Final element is a north star ([inspired by 1901 Maine flag](https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Flag_of_Maine_%281901-1909%29.svg/1024px-Flag_of_Maine_%281901-1909%29.svg.png)).

I also created a [remastered/redesigned version of 1901 Maine flag](https://raw.githubusercontent.com/Az-21/flags/master/States/Maine/maine-1901-remastered.png). Hope you like it.

And this is the result. Up next is Maryland (God help me).

!wave

---

### Additional Information

I'm redesigning the state flags of USA because why not. Share your opinion and comments, it'll help me greatly. 

[Catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.

!wave