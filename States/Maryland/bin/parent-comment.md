Current flag of [Maryland](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Flag_of_Maryland.svg/1024px-Flag_of_Maryland.svg.png)

---

Design Process

Current flag of Maryland is amazing. It generates conflicting opinions within the community, but you can't deny it's a striking flag. Maryland flag is like Nigeria's 2018 football WC kit, breaks the rule of simplicity for simething bizzare which shouldn't work but it does. I love it.

For my redesign, I used the symbolism of Maryland's state flower: black-eyed susan. Background is inspired by the geography of Maryland.

And this is the result. Up next is Massachusetts.

---

[Catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.