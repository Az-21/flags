### Current flag of [Georgia](https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Flag_of_Georgia_%28U.S._state%29.svg/1280px-Flag_of_Georgia_%28U.S._state%29.svg.png)

---

### Design Process
Current flag of Georgia is good apart from the issue of text. One significant problem I have is that the caton is not squared for some reason; it's not referencing any other flag, it's just the coat of arms of Georgia and they could have squared that portion.

For my redesign I went for saturated American colors, 3 pillars & stars signifing wisdon, justice, and moderation. Honestlly, I'm not satisfied with my own redesign, it's missing something.

But here it is anyways. Up next is Hawaii.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them. If you're not using Illustrator, I'll export my redesign as .svg and add it to the catalog; just send me a message. 

!wave