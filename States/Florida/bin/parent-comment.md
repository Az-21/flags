### Current flag of [Florida](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Flag_of_Florida.svg/1024px-Flag_of_Florida.svg.png)

---

### Design Process
Current flag of Florida is basically Alabama with a seal printed on it. This flag also falls for complex imagery and text. However, the real tradegy is that Florida didn't even bother to change/remove the text; it literally says "great **seal** of Florida" on a map! That's just lazy and unfortunate.

For my redesign, I considered the flowers seen on the seal, but then I decided to use the orange blossom as central element. Oranges of Florida are world renouned, and I think it's a great symbolism for state.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them. If you're not using Illustrator, I'll export my redesign as .svg and add it to the catalog; just send me a message. 

!wave