Current flag of [Minnesota](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Flag_of_Minnesota.svg/1024px-Flag_of_Minnesota.svg.png)

---

**Design Process**

There are 3 elements in my redesign. First one is a star on caton to represent the state's motto: Star of the North. Second is the sky blue color (same as current flag) which signifies the meaning of Minnesota: "sky-tinted water." Third, and final, element is equally spaced squares to signify the lakes of Minnesota, there's more than 10,000 lakes there.
