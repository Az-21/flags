### Current flag of [Louisiana](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Flag_of_Louisiana.svg/1000px-Flag_of_Louisiana.svg.png)

---

### Design Process

Current flag of Louisiana is quite complex, nevertheless quite good barring the motto. The shade of blue used is also great; most US states go for this eye hurting deep dark blue.

For my redesign, I kept the symbolism of pelican, but I dropped her kids. The background is half blue (current shade) and a complementing orange. Pelican takes the center stage and the background is adjusted according to pelican to avoid the "Bhutan stituation."

And this is the result. Up next is Maine.

---

### Additional Information

I'm redesigning the state flags of USA because why not. Share your opinion and comments, it'll help me greatly. 

[Catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.

!wave