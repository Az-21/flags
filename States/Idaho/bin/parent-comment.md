### Current flag of [Idaho](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_Idaho.svg/1280px-Flag_of_Idaho.svg.png)

### [Coat of arms referenced](https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Idaho_territory_coat_of_arms_%28illustrated%2C_1876%29.jpg/727px-Idaho_territory_coat_of_arms_%28illustrated%2C_1876%29.jpg)

---

### Design Process
Current flag of Idaho is yet another case of seal printed on a bedsheet. They didn't even bother to remove the word "seal" from the flag.

For my redesign I used colors from the historical coat of arms of Idaho: washed up shades of blue and red. At first, I chose to include an eagle, but it didn't work out. Then I refereced the current flag for symbolism of stag/deer, and I used this badass stag courtesy [freepik](https://www.freepik.com/free-vector/ethnic-deer_814011.htm) to highlight the cultural significance of deers in Native American Idaho and modern Idaho. This is my favourite redesign despite being incredibly complex, I hope you like it.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them. If you're not using Illustrator, I'll export my redesign as .svg and add it to the catalog; just send me a message. 

!wave