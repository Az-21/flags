Current flag of [Arizona](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Flag_of_Arizona.svg/1024px-Flag_of_Arizona.svg.png)

---
### Design Process

I don't know why, but I get communist vibes from this flag. The coat of arms of Arizona isn't impressive. It's all irregular shapes, hence it's hard to extract someing uniquely symbolic to Arizona. I used the color pallet of the coat of arms to signify sky, grand canyon, and sun in this flag.
---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.