Current flag of [Missouri](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Flag_of_Missouri.svg/1280px-Flag_of_Missouri.svg.png)

---

**Design Process**

My redesign features the bear (symbolizes bravery) and crescent moon (symbolizes potential for growth) as seen on the coat of arms of Missouri. Color scheme follows the current flag minus the white band. I borrowed the from flag of California because Missouri's bear is terribly designed and looks bloated and deformed.