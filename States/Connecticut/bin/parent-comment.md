### Current flag of [Connecticut](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Flag_of_Connecticut.svg/994px-Flag_of_Connecticut.svg.png)

---
### Design Process

The current flag of Connecticut is a classic case of seal printed on a bedsheet; to their credit, they at least changed the shield. It also suffers from the problem of text and complex imagery which is illegible from a distance.

For my redesign, I went for the symbolism of grapes which is featured on current state flag and state's coat of arms to a psycotic degree. I hope the white band is visible on reddit's light theme (why do you even use it? RIP your eyes).

And here's the result. Up next is Delaware (sssmAAAssshh).

---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.