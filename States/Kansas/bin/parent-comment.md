### Current flag of [Kansas](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Flag_of_Kansas.svg/1280px-Flag_of_Kansas.svg.png)

---

### Design Process

Current flag of Kansas is textbook seal on a bedsheet. Text, complex imagery, seal, too many stars,  and inconsistent color pallet, you got it all.

For my redesign, I used the meaning of Kansas Native Amerian tribe (the tribe state is named after) where Kansas means people of the wind. Flags with moving elements usually go towards hoist to symbolize stability, but here it didn't feel right; hence, wind blows towards right. Background is a softer blue, you know....sky blue.

And this is the result. Up next is Kentucky.

---

### Additional Information

I'm redesigning the state flags of USA because why not. Share your opinion and comments, it'll help me greatly. 

[Catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.

!wave