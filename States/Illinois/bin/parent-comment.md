### Current flag of [Illinois](https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Flag_of_Illinois.svg/1280px-Flag_of_Illinois.svg.png)

---

### Design Process

Current flag of Illinois is yet another case of seal on a bedsheet. At the very least, Illinois managed to remove the word "seal" from their flag unlike some states.

For my redesign, I developed the rising sun hidden behind the eagle. Used the same color pallet of blue for water, orange for sun, and yellow for sunrays. I changed the style of sunrays to achieve a symmetric design.

And this is the result. Up next is Indiana.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.

!wave