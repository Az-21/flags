Current flag of [Montana](https://upload.wikimedia.org/wikipedia/commons/c/cb/Flag_of_Montana.svg)

---

**Design Process**

This is my favourite flag so far. It's inspired by Montana's state flower: [bitterroot](https://cdn.history.com/sites/2/2013/11/MT-state-flower-bitterroot.jpg). Background is pure white to give this flower the center stage it deserves.