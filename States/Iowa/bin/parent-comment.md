### Current flag of [Iowa](https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Flag_of_Iowa.svg/1000px-Flag_of_Iowa.svg.png)

---

### Design Process

Current flag of Iowa features unequally spaced France's flag, motto and text which is illegible from a distance, and an eagle with a twisted neck (he's dead Jim).

For my redesign, I used an acorn because oak is the state flag of Iowa. Base of falg is two tones of green to represent the agriculture of Iowa: about 90% land is dedicated towards agriculture.

And this is the result. Up next is Idaho.

---

### Additional Information

I'm redesigning the state flags of USA because why not. Share your opinion and comments, it'll help me greatly. 

[Catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them.

!wave