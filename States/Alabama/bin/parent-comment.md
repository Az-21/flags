Current flag of [Alabama](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Alabama.svg/1024px-Flag_of_Alabama.svg.png).

---
### Design Process

Alabama is basically an unwelcoming red X. Fortunately, the color pallet is simplisitc and beautiful. Here's the detailed process

* Remove the cross.
* Create two right angled triangles with left edge as base, and middle of top edge as height. Repeat with right edge. This results in a positive slope in the whitespace.
* Extract the US styled shield from [Alabama's coat of arms](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Coat_of_arms_of_Alabama.svg/1000px-Coat_of_arms_of_Alabama.svg.png). Simplify it to use 3 red bands. I tested this shield with different edges, but it looks cleanest without edges.

And this is the result. Up next is Alaska.

---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other, or just hurt the eye.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.