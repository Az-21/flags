Current flag of [Alaska](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Flag_of_Alaska.svg/1024px-Flag_of_Alaska.svg.png)

---
### Design Process

Alaska has one of the okay-ish flags. There are no glaring issues so it's quite hard to improve it while preserving the current identity. Here's the detailed process:

* Change the color pallet because dark blue-indigo does not go well with yellow. No matter what the combination, dark blue makes the composition off balance. Therefore, I went for light tone of skyblue.
* Alaska has a poor coat of arms (seal); not a single element is a proper shape: it's all irregular mountains, aurora, and fields.
* I still decided to follow the coat of arms and created white symmetric mountains.
* The aurora is an eyecatching element. I added 3 thin horizontal bands with different shades of cyan to represent aurora. Finally, I decided to drop it because having 6 colors in a flag makes it complex. I still liked the idea, so I just used one horizontal band and colored it yellow.
* To preserve some identity, I added a nautical star in the top-right corner to represent north star.

Here's the result. Up next is Arizona.

---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.