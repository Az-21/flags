### Current flag of [Delaware](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Flag_of_Delaware.svg/1024px-Flag_of_Delaware.svg.png)

### Current [coat of arms/seal of Delaware](https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Seal_of_Delaware.svg/1000px-Seal_of_Delaware.svg.png)

---

### Design Process
Current flag of Delaware is another case of seal on a bedsheet. Like many other state flags, it has complex imagery and motto which is illegible from a distance. They even chose to print on a date with a terrible typeface for numbers; even Futura is bad in that regard so I'll give them a pass.

For my redesign, I stuck to the central element of the seal: a horizontal tricolor with a bull (or is it a cow, I can't tell). I also played with the idea of corn and hay, and ship and waves in my drafts, but the simple tricolor was victorious.

And this is the result. Up next is Florida.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them. If you're not using Illustrator, I'll export my redesign as .svg and add it to the catalog; just send me a message. 

!wave