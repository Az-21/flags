### Current flag of [Hawaii](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Flag_of_Hawaii.svg/1280px-Flag_of_Hawaii.svg.png)

---

### Design Process

Current flag of Hawaii features Union Jack, and I'm not sure why that's the case. Anyhow, it's a decent flag, and I had a some ideas to improve it.

For my redesign, I referenced [Hawaii's state tree: Kukui](https://en.wikipedia.org/wiki/Aleurites_moluccanus) for the central element. Then, stylized the bottom of the leaf like a mountain because Hawaii is technically a mountain range peaking out of the ocean. Finally, I used 3 bands to symbolize ocean.

And this is the result. Up next is Idaho.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them. If you're not using Illustrator, I'll export my redesign as .svg and add it to the catalog; just send me a message. 

!wave