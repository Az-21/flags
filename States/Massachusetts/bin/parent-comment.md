Current flag of [Massachusetts](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Massachusetts.svg/1500px-Flag_of_Massachusetts.svg.png)

---

Design Process

Current flag of Massachusetts features a Native Indian holding a bow and arrow. I stylized this element for my redesign. It features a horizontal band symbolizing a bow, and an arrow. Final element is a 6 point star because MA was the 6th state to join the Union.