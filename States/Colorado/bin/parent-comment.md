### Current flag of [Colorado](https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colorado_designed_by_Andrew_Carlisle_Carson.svg/1024px-Flag_of_Colorado_designed_by_Andrew_Carlisle_Carson.svg.png)

---
### Design Process
They had to include a letter, didn't they? Well, at least it's not a seal on a bedsheet, and apart from "the issue of C" it's a good flag with a complimenting color pallet. For the redesign, I reduced the size of blue horizontal bands, centered the sun, removed "C," and stylized the sun. It looks like the sun is emmiting solar flares, but I'd take solar flares over letters in a flag any day.

---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.