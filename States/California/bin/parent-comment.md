### Current flag of [California](https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Flag_of_California.svg/1280px-Flag_of_California.svg.png)

---
### Design Process

I like the flag of California. It has text and complex imagery, but it's nevertheless a great flag. For my redesign, I used red shade from the original flag; thicked the red horizontal bar and duplicated it at the top edge. The three stars represent the three eras Cali has witnessed: Native Indian era, Mexican era, and Modern era.

And here's the result. Up next is Colarado.
---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.