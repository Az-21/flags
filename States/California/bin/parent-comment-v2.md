### Current flag of [California](https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Flag_of_California.svg/1000px-Flag_of_California.svg.png)

---

### Design Process
Yesterday, I posted my [original redesign](https://www.reddit.com/r/vexillology/comments/92d81e/state_flag_of_california_usa_redesigned/). It looked like flag of DC, and I was unaware of it. So, here is state flag of California, USA redesigned v2. It still features 3 stars symbolizing the 3 eras California has witnessed: Native American, Mexican, and Modern.

---

### Additional Information

For a personal project, I dediced redesign the state flags of USA. I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements and post them. If you're not using Illustrator, I'll export my redesign as .svg and add it to the catalog; just send me a message. 

!wave