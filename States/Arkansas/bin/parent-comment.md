### Current flag of [Arkansas](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Flag_of_Arkansas.svg/640px-Flag_of_Arkansas.svg.png)

---
### Design Process

I believe the diamond design stands out, but it's held back by text, too many stars, and unbalanced colors (too much red). The shade of red used is too bright and threating for my liking.

My redesign features a subdued shade of red, arrow, and grain form Arkansa's coat of arms. The final element is a Greenland style diamond.

And this is the result. Up next is California.

---
### Additional Information

For a personal project, I dediced to try to improve the state flags of USA. Here are some of the rules I followed: 

* Use the same color pallet as the current flag, unless the colors do not compliment each other.
* Maintain a universal 5:3 ratio.
* Although text on flag is vastly superior to symbolism, I will avoid it. Congress of US Flags, my sincerest apologies.

I'm maintaining a [catalog on GitHub](https://github.com/Az-21/flags/tree/master/States). Feel free to make improvements.